import PropTypes from 'prop-types'
import React from 'react'
import { TabNavigator } from 'react-navigation'
import { TAB } from './config/tabs'
import { withProps } from './lib/decorators'
import { isEmpty } from './lib/object'
import LoginScreen from './screens/LoginScreen'
import MapScreen from './screens/MapScreen'
import ProfileScreen from './screens/ProfileScreen'
import RegisterScreen from './screens/RegisterScreen'
import SplashScreen from './screens/SplashScreen'
import TaskScreen from './screens/TaskScreen'
import TasksScreen from './screens/TasksScreen'

const TABS = {
  [TAB.TASK]    : { screen: TaskScreen },
  [TAB.TASKS]   : { screen: TasksScreen },
  [TAB.MAP]     : { screen: MapScreen },
  [TAB.PROFILE] : { screen: ProfileScreen },
  [TAB.LOGIN]   : { screen: LoginScreen },
  [TAB.REGISTER]: { screen: RegisterScreen }
}

const FAIL_BACK_TAB = {
  [TAB.WELCOME]: { screen: SplashScreen }
}

export default class Router extends React.Component {
  static propTypes = {
    tabs: PropTypes.object
  }

  @withProps
  getTabs({ tabs }) {
    const keys = Object.keys(tabs)
    const enabled = keys.filter(k => tabs[k])
    const screens = isEmpty(enabled) ? { ...FAIL_BACK_TAB } : {}

    enabled.forEach(t => screens[t] = TABS[t])
    const Tabs = TabNavigator(screens)
    return <Tabs/>
  }

  shouldComponentUpdate({ tabs }) {
    return JSON.stringify(tabs) !== JSON.stringify(this.props.tabs)
  }

  render() { return this.getTabs() }
}
