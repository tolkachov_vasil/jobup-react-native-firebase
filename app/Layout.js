import PropTypes from 'prop-types'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import { detectLocation } from './actions/map'
import { autoLoginUserRequest } from './actions/user'
import Flash from './components/Flash'
import { withProps } from './lib/decorators'
import { ActionType } from './lib/types'
import Router from './Router'

@connect(
  ({ tabs }) => ({ tabs }),
  { autoLoginUserRequest, detectLocation })
export default class Layout extends React.Component {
  static propTypes = {
    tabs                : PropTypes.object,
    detectLocation      : ActionType,
    autoLoginUserRequest: ActionType
  }

  @withProps
  componentWillMount({ detectLocation, autoLoginUserRequest }) {
    detectLocation()
    autoLoginUserRequest()
  }

  @withProps
  render({ tabs }) {
    return (
      <View style={{ ...StyleSheet.absoluteFillObject }}>
        <Flash/>
        <Router tabs={tabs}/>
      </View>
    )
  }
}
