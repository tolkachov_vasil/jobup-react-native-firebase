import PropTypes from 'prop-types'
import React from 'react'
import combineStyles from 'react-combine-styles'
import { StyleSheet, Text as NativeText } from 'react-native'
import { color, size } from '../../config/styles'

const Text = ({
                value,
                children,
                block,
                center,
                gray,
                red,
                gapBottom,
                gapTop,
                spaced,
                smaller,
                larger,
                largest
              }) => {

  const style = combineStyles({
    block,
    center,
    gray,
    red,
    gapBottom,
    gapTop,
    spaced,
    smaller,
    larger,
    largest
  }, styles)

  return (
    <NativeText
      style={style}
    >
      {value || children}
    </NativeText>
  )
}

const styles = StyleSheet.create({
  block    : {
    flexGrow: 1
  },
  center   : {
    textAlign: 'center'
  },
  gray     : {
    color: color.gray
  },
  red      : {
    color: color.red
  },
  gapBottom: {
    marginBottom: 10
  },
  gapTop   : {
    marginTop: 10
  },
  spaced   : {
    letterSpacing: 1.5
  },
  smaller  : {
    fontSize: size.font.smaller
  },
  larger   : {
    fontSize: size.font.larger
  },
  largest  : {
    fontSize: size.font.largest
  }
})

Text.propTypes = {
  value    : PropTypes.string,
  block    : PropTypes.bool,
  center   : PropTypes.bool,
  gray     : PropTypes.bool,
  red      : PropTypes.bool,
  gapBottom: PropTypes.bool,
  gapTop   : PropTypes.bool,
  spaced   : PropTypes.bool,
  smaller  : PropTypes.bool,
  larger   : PropTypes.bool,
  largest  : PropTypes.bool
}

export default Text
