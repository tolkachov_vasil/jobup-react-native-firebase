import PropTypes from 'prop-types'
import React from 'react'

export const Show = ({ when, children }) => when ? children : null
export const Hide = ({ when, children }) => when ? null : children

Show.propTypes = {
  when: PropTypes.bool
}

Hide.propTypes = {
  when: PropTypes.bool
}
