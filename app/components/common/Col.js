import PropTypes from 'prop-types'
import React from 'react'
import combineStyles from 'react-combine-styles'
import { StyleSheet, View } from 'react-native'

const Col = ({
               larger,
               right,
               gapLeft,
               gapRight,
               expanded,
               children
             }) => {
  const style = combineStyles({
    back: true,
    larger,
    right,
    gapLeft,
    gapRight,
    expanded
  }, styles)

  return (
    <View style={style}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  back: {
    flexShrink: 1
  },

  larger: {
    flexGrow: 0.5
  },

  expanded: {
    flexGrow: 1,
    width   : 0
  },

  right: {
    textAlign: 'right'
  },

  gapLeft: {
    marginLeft: 10
  },

  gapRight: {
    marginRight: 10
  }
})

Col.propTypes = {
  right   : PropTypes.bool,
  larger  : PropTypes.bool,
  gapLeft : PropTypes.bool,
  gapRight: PropTypes.bool,
  expanded: PropTypes.bool
}

export default Col
