import PropTypes from 'prop-types'
import React from 'react'
import combineStyles from 'react-combine-styles'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import { color } from '../../config/styles'

const Button = ({
                  caption,
                  children,
                  onPress = f => f,
                  disabled,
                  red,
                  larger
                }) => {

  const style = combineStyles({
    back: !children,
    larger,
    disabled,
    red
  }, styles)

  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={() => onPress()}
    >
      {children
        ? children
        : <Text style={style}> {caption} </Text>
      }
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  back: {
    color       : color.active,
    marginTop   : 10,
    marginBottom: 10
  },

  disabled: {
    opacity: .5
  },

  red: {
    color: color.red
  },

  larger: {
    fontSize: 18
  }
})

Button.propTypes = {
  caption : PropTypes.string,
  onPress : PropTypes.func,
  disabled: PropTypes.bool,
  larger  : PropTypes.bool
}

export default Button
