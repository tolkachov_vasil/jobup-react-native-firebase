import PropTypes from 'prop-types'
import React from 'react'
import combineStyles from 'react-combine-styles'
import { StyleSheet, View } from 'react-native'

const Row = ({
               wrap,
               start,
               children
             }) => {
  const style = combineStyles({
    back: true,
    wrap,
    start
  }, styles)

  return (
    <View style={style}>
      {children}
    </View>
  )
}

const styles = StyleSheet.create({
  back : {
    display       : 'flex',
    flexDirection : 'row',
    justifyContent: 'space-between',
    flexWrap      : 'nowrap'
  },
  wrap : {
    flexWrap: 'wrap'
  },
  start: {
    justifyContent: 'flex-start'
  }
})

Row.propTypes = {
  wrap : PropTypes.bool,
  start: PropTypes.bool
}

export default Row
