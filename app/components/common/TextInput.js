import PropTypes from 'prop-types'
import React from 'react'
import combineStyles from 'react-combine-styles'
import {
  StyleSheet,
  Text,
  TextInput as NativeTextInput,
  View
} from 'react-native'
import { color, size } from '../../config/styles'

const TextInput = ({
                     value,
                     name,
                     password,
                     disabled,
                     placeholder,
                     invalid,
                     errors,
                     onChange = f => f
                   }) => {

  const isInvalid = !!value && !!errors

  const style = combineStyles({
    back   : true,
    invalid: invalid || isInvalid,
    disabled
  }, styles)

  const error = Array.isArray(errors) ? errors.join(', ') : null

  return (
    <View>
      <NativeTextInput
        style={style}
        value={value}
        secureTextEntry={password}
        placeholder={placeholder}
        disabled={disabled}
        underlineColorAndroid='transparent'
        onChangeText={text => onChange(name ? { [name]: text } : text)}
      />
      {error &&
       <Text style={styles.errorMsg}>{error}</Text>}
    </View>
  )
}

const styles = StyleSheet.create({
  back    : {
    borderWidth : 1,
    borderColor : color.gray,
    padding     : 5,
    marginBottom: 20,
    marginTop   : 10
  },
  invalid : {
    borderColor: color.red
  },
  disabled: {
    opacity: 0.2
  },
  errorMsg: {
    position : 'absolute',
    color    : color.red,
    fontSize : size.smallest,
    marginTop: 40
  }
})

TextInput.propTypes = {
  value      : PropTypes.string,
  name       : PropTypes.string,
  placeholder: PropTypes.string,
  disabled   : PropTypes.bool,
  invalid    : PropTypes.bool,
  password   : PropTypes.bool,
  errors     : PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  onChange   : PropTypes.func
}

export default TextInput
