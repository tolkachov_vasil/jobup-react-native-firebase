import PropTypes from 'prop-types'
import React from 'react'
import { StyleSheet } from 'react-native'
import MapView from 'react-native-maps'
import { LocationType } from '../lib/types'
import { Marker } from './common/SVG/Marker'

const GMap = ({ location, markers = [], onSelect }) => {
  return (
    <MapView
      moveOnMarkerPress
      showsUserLocation
      style={StyleSheet.absoluteFill}
      region={location}
      onPress={e => onSelect({ location: e.nativeEvent.coordinate })}
    >
      {markers.map(m =>
        <MapView.Marker
          onPress={e => {
            e.stopPropagation()
            onSelect({ marker: m })
          }}
          coordinate={m}
          key={m.id}
          children={<Marker/>}
        />)}
    </MapView>
  )
}

GMap.propTypes = {
  location: LocationType,
  markers : PropTypes.arrayOf(LocationType),
  onSelect: PropTypes.func.isRequired
}

export default GMap
