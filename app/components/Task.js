import PropTypes from 'prop-types'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { color } from '../config/styles'
import { formattedDate } from '../lib/date'
import { taskTitle } from '../lib/task'
import { TaskType } from '../lib/types'
import { Button, Col, Row, Text } from './common'
import { Pencil, Trash } from './common/SVG/ActionIcons'

export default class Task extends React.Component {
  static propTypes = {
    value   : TaskType,
    onDelete: PropTypes.func.isRequired,
    onSelect: PropTypes.func.isRequired,
    onEdit  : PropTypes.func.isRequired
  }

  render() {
    const { value, onDelete, onEdit, onSelect } = this.props

    return (
      <View style={styles.root}>
        <Button onPress={() => onSelect(value)}>
          <Row>
            <Col expanded>
              <Text value={formattedDate(value.createdAt)} gray smaller/>
              <Text value={taskTitle(value)}/>
            </Col>
            <Col gapLeft gapRight>
              <Button onPress={() => onEdit(value)}>
                <Pencil color={color.active}/>
              </Button>
            </Col>
            <Col>
              <Button onPress={() => onDelete(value)}>
                <Trash color={color.active}/>
              </Button>
            </Col>
          </Row>
        </Button>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  root: {
    marginLeft    : 20,
    marginRight   : 20,
    paddingTop    : 20,
    paddingBottom : 20,
    borderTopColor: '#ddd',
    borderTopWidth: 1
  }
})
