import PropTypes from 'prop-types'
import React, { Component } from 'react'
import combineStyles from 'react-combine-styles'
import { StyleSheet, Text, View } from 'react-native'
import { connect } from 'react-redux'
import { hideFlash } from '../actions/flash'
import { FAIL, SUCCESS } from '../config/flash'
import { color, size } from '../config/styles'
import { withProps } from '../lib/decorators'
import { objectToString } from '../lib/object'
import { ActionType } from '../lib/types'
import { Button } from './common'
import { Trash } from './common/SVG/ActionIcons'

@connect(
  ({ flash }) => ({ flash }),
  { hideFlash })
export default class Flash extends Component {
  static propTypes = {
    flash    : PropTypes.arrayOf(PropTypes.shape({
      id  : PropTypes.number,
      text: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
      ]),
      kind: PropTypes.oneOf([SUCCESS, FAIL])
    })),
    hideFlash: ActionType
  }

  @withProps
  render({ flash, hideFlash }) {
    return (
      <View style={styles.root}>
        {flash.map(f =>
          this.renderFlash(f, hideFlash))}
      </View>)
  }

  renderFlash({ id, text, kind }, hideFlash) {
    const isSuccess = kind === SUCCESS

    const style = combineStyles({
      back   : true,
      success: isSuccess,
      fail   : !isSuccess
    }, styles)

    const value = typeof text === 'string' ? text : objectToString(text)

    return (
      <View style={style} key={id}>
        <Text style={styles.text} children={value}/>
        <Button onPress={() => hideFlash(id)} children={
          <Trash/>}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  root   : {
    position     : 'absolute',
    zIndex       : 100,
    top          : 0,
    left         : 0,
    right        : 0,
    flexDirection: 'column'
  },
  back   : {
    flexGrow      : 1,
    opacity       : 0.9,
    marginTop     : 10,
    marginLeft    : 10,
    marginRight   : 10,
    padding       : 10,
    borderRadius  : size.borderRadius,
    flexDirection : 'row',
    justifyContent: 'space-between',
    flexWrap      : 'nowrap'
  },
  text   : {
    flexShrink: 1,
    fontSize  : size.font.largest,
    flexGrow  : 1
  },
  success: {
    backgroundColor: color.success
  },
  fail   : {
    backgroundColor: color.fail
  }
})
