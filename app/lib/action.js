export const createAction = (type, ...keys) => (...args) => {
  const action = { type }
  keys.forEach((_, i) => action[keys[i]] = args[i])
  action.__FROM = getCaller()
  return action
}

function getCaller(level: number = 3): string {
  let result = ''
  try { throw new Error() }
  catch (e) {
    result = e.stack
      .split(/\n/g)
      [level]
      .replace(/^\s+at\s+(\S+)\s+.+$/g, '$1')
  }
  return result
}
