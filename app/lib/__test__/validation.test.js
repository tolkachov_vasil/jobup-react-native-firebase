import { is } from '../validation'

describe('validation', () => {

  it('single value validation', () => {
    const errors = is
      .arrayOf(is.number().positive())
      .getErrors([1, 3, -3, 'sdfg'])
    expect(errors).toEqual(
      [{ '2': ['must be positive'], '3': ['not number'] }]
    )
  })

  it('nested object validation', () => {
    const schema = is.shape({
      customTest         : is.test(v => v < 20),
      outer1             : is.number().positive(),
      outer2             : is.string(),
      yesOrNoValid       : is.oneOfValue(['yes', 'no']),
      yesOrNoInvalid     : is.oneOfValue(['yes', 'no']),
      notObj             : is.shape(),
      array              : is.arrayOf(is.number().positive()),
      notArray           : is.arrayOf(is.number().positive()).required(),
      stringOrNumber1    : is.oneOfType([is.number(), is.string()]),
      stringOrNumber2    : is.oneOfType([is.number(), is.string()]),
      stringOrNumberWrong: is.oneOfType([is.number(), is.string()]),

      object: is.shape({
        inner1: is.string(),
        inner2: is.string(),

        inner3: is.shape({
          deep1        : is.number(),
          deep2        : is.string().required(),
          rootTest     : is.testRoot((root, me) => root.outer2 === me),
          rootTestWrong: is.testRoot((root, me) => root.outer2 === me)
        })
      })
    })

    const obj = {
      customTest         : 45,
      outer1             : '-5',
      outer2             : 12,
      yesOrNoValid       : 'no',
      yesOrNoInvalid     : 12,
      stringOrNumber1    : 56,
      stringOrNumber2    : 'dfghfdgh',
      stringOrNumberWrong: { d: 5 },
      notObj             : null,
      array              : [1, -2, 3, 4, '5'],
      object             : {
        inner1: 'inner1_value',
        inner2: 100,
        inner3: {
          deep1        : 'deep_text',
          rootTest     : 12,
          rootTestWrong: 13
        }
      }
    }
    expect(schema.getErrors(obj))
      .toEqual(
        {
          'array'     : [{
            '1': ['must be positive'],
            '4': ['not number']
          }],
          'customTest': ['error'],
          'notArray'  : ['not array', 'required'],
          'notObj'    : ['not object'],

          'object': {
            'inner1': null,
            'inner2': ['not string'],

            'inner3': {
              'deep1'        : ['not number'],
              'deep2'        : ['required'],
              'rootTest'     : null,
              'rootTestWrong': ['error']
            }
          },

          'outer1'             : ['not number', 'must be positive'],
          'outer2'             : ['not string'],
          'stringOrNumber1'    : null,
          'stringOrNumber2'    : null,
          'stringOrNumberWrong': ['invalid variant'],
          'yesOrNoInvalid'     : ['only yes/no'],
          'yesOrNoValid'       : null
        }
      )
  })
})

