import { path } from '../path'

describe('make path', () => {

  it('interpolate 2 values from object', () => {
    const result = path('/asd/:id/qwe/:userId/1', { id: 123, userId: 345 })
    expect(result).toEqual('/asd/123/qwe/345/1')
  })

  it('interpolate 1 value from second argument', () => {
    const result = path('/asd/:id', 12345)
    expect(result).toEqual('/asd/12345')
  })

})
