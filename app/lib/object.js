export const isObject = v => v === Object(v)

export const isEmpty = v => Object.keys(v).length === 0

export const objectToString = v => JSON.stringify(v)
  .replace(/"|{|}/g, '')
  .replace(/,/g, ', ')

export const getFrom = (object = {}, keysString = '') => {
  const newObject = {}

  keysString.split(' ').forEach(k => {
    if (object[k] !== null && object[k] !== undefined) { newObject[k] = object[k] }
  })

  return newObject
}
