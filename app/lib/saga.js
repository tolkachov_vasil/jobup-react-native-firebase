import { Alert } from 'react-native'
import { call, put, select } from 'redux-saga/effects'
import { showFlash } from '../actions/flash'
import { hideTab, showTab } from '../actions/tabs'
import { FAIL } from '../config/flash'
import { isObject, objectToString } from './object'

export function confirmed(message = 'Are you sure?') {
  return new Promise(resolve => {
    Alert.alert(
      message,
      '',
      [
        { text: 'Cancel', onPress: () => resolve(false), style: 'cancel' },
        { text: 'OK', onPress: () => resolve(true) }
      ],
      { cancelable: false }
    )
  })
}

export function* setTab(name, value) {
  const tabs = yield select(s => s.tabs)

  if (tabs[name] !== value) {
    yield put(value ? showTab(name) : hideTab(name))
  }
}

export function* attempt(fn, ...args) {
  attempt.fail = false
  attempt.ok = true
  attempt.error = null
  try {
    return yield call(fn, ...args)
  } catch (error) {
    attempt.fail = true
    attempt.ok = false
    attempt.error = error
    yield put(showFlash(FAIL, formatError(error)))
  }
}

function formatError(data) {
  if (isObject(data)) {
    return data.message
      ? data.message
      : objectToString(data)
  }
  return data
}
