let rootValue = undefined

export const is = {

  shape(schema = {}, error = 'not object') {
    return this._newInstance(schema, v => v !== Object(v) ? error : null)
  },

  arrayOf(schema = {}, error = 'not array') {
    const chain = [...schema.__CHAIN__]

    function testArray(v) {
      if (!Array.isArray(v)) { return error }
      const errors = {}

      v.forEach((val, i) => {
        const error = chain.map(fn => fn.call(this, v[i])).filter(r => !!r)
        if (error.length > 0) { errors[i] = error }
      })
      return errors
    }

    return this._addFn(testArray)
  },

  oneOfValue(values = [], error) {
    const variantsNum = values.length
    let errorsNum = 0

    function isOneOfValue(v) {
      values.forEach(val => {
        if (v !== val) { errorsNum++ }
      })
      return variantsNum === errorsNum ? error || (`only ${values.join('/')}`) : null
    }

    return this._newInstance({}, isOneOfValue)
  },

  oneOfType(schemas = []) {
    const schema = {}
    const chains = schemas.map(s => s.__CHAIN__)
    const variantsNum = chains.length
    let errorsNum = 0

    const isOneOfType = v => {
      chains.forEach(chain => {
        const error = chain.map(fn => fn.call(this, v)).filter(r => !!r)
        if (error.length > 0) { errorsNum++ }
      })
      return variantsNum === errorsNum ? 'invalid variant' : null
    }

    return this._newInstance(schema, isOneOfType)
  },

  number(error = 'not number') {
    return this._addFn(v => !!v && typeof v !== 'number' ? error : null)
  },

  positive(error = 'must be positive') {
    return this._addFn(v => !!v && parseInt(v, 10) < 0 ? error : null)
  },

  string(error = 'not string') {
    return this._addFn(v => !!v && typeof v !== 'string' ? error : null)
  },

  bool(error = 'not boolean') {
    return this._addFn(v => !v || typeof v === 'boolean' ? null : error)
  },

  test(fn, error = 'error') {
    return this._addFn(v => fn(v) ? null : error)
  },

  testRoot(fn, error = 'error') {
    return this._addFn(
      function testWithRoot(v) {
        return fn(rootValue, v) ? null : error
      })
  },

  minLength(min, error = 'min length ') {
    return this._addFn(v => !v || v.toString().length >= min ? null : error + min)
  },

  email(error = 'invalid email') {
    return this._addFn(v => !v || /^.+@.+\..+$/.test(v) ? null : error)
  },

  required(error = 'required') {
    return this._addFn(v => v === undefined || v === null || v === '' ? error : null)
  },

  getErrors(value, schema = this, errors = {}) {
    if (rootValue === undefined) {
      rootValue = value
    }

    Object.keys(schema).forEach(key => {
      if (key === '__CHAIN__') {
        const errs = schema.__CHAIN__.map(fn => fn.call(this, value))
          .filter(v => !!v)
        if (errs.length > 0) { errors = errs }
      } else {
        errors[key] = this.getErrors(value[key], schema[key], errors[key])
      }
    })

    return Object.keys(errors).length > 0 ? errors : null
  },

  _newInstance(schema = {}, fn) {
    Object.setPrototypeOf(schema, this)
    rootValue = undefined
    schema.__CHAIN__ = fn ? [fn] : []

    return schema
  },

  _addFn(fn) {
    if (!this.__CHAIN__) {
      return this._newInstance({}, fn)
    } else {
      this.__CHAIN__.push(fn)
      return this
    }
  }
}
