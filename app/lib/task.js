export const taskTitle = ({ service = '', work = '', description = '' }) =>
  'I need a ' + (!!service ? service.toLowerCase() : '')
  + (!!work ? ' to ' + work.toLowerCase() : '...')
  + (!!description ? ', ' + description : '.')
