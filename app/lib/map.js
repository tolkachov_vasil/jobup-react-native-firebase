import { GOOGLE_MAP_API_KEY } from '../config/google'

export const getMyLocation = () =>
  new Promise((resolve, reject) =>
    navigator.geolocation.getCurrentPosition(
      position =>
        resolve({
          latitude : position.coords.latitude,
          longitude: position.coords.longitude
        }),
      error => reject(error),
      { enableHighAccuracy: true, timeout: 1000, maximumAge: 1000 }
    ))

const GM_API_URL = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='

export const getAddressByLocation = ({ latitude, longitude }) => {
  const url = `${GM_API_URL}${latitude},${longitude}&key=${GOOGLE_MAP_API_KEY}`

  return new Promise((resolve, reject) => {
    return fetch(url, {
      method     : 'GET',
      credentials: 'omit',
      headers    : { Accept: 'application/json' }
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error))
  })
}
