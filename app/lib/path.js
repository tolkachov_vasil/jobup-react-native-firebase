export const path = (template, values) =>
  template.replace(/(:[\w\d_]+)/g, (_, k) => values[k.slice(1)] || values)
