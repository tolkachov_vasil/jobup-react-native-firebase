import { getFrom } from './object'

export const taskDTO = data =>
  getFrom(data, 'service work description createdAt address latitude longitude')

export const userDTO = data =>
  getFrom(data, 'email name uid emailVerified')

