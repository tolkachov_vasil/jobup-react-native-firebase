import React, { Component } from 'react'

export function withProps(target, name, descriptor) {
  return connected(descriptor, 'props', target)
}

export function withState(target, name, descriptor) {
  return connected(descriptor, 'state', target)
}

export function withPropsAndState(target, name, descriptor) {
  return connected(descriptor, 'all', target)
}

function connected(descriptor, key, target) {
  if (descriptor.value) {
    return connectedMethod(descriptor, key)
  }
  const decorator = key[0].toUpperCase() + key.slice(1)
  const className = target.constructor.name
  throw `${className}: decorator @with${decorator} only for methods`
}

function connectedMethod(descriptor, key) {
  return key === 'all'
    ?
         {
           ...descriptor,
           value: function wrapper(...args) {
             return descriptor.value.apply(this, [this.props, this.state, ...args])
           }
         }
    :
         {
           ...descriptor,
           value: function wrapper(...args) {
             return descriptor.value.apply(this, [this[key], ...args])
           }
         }
}

// blueprint
const setTitle = (getTitle) => (WrappedComponent) => {
  return class extends Component {
    updateTitle = (props) => {
      // Check if the callback has returned something,
      // and if so - update the title
      const title = getTitle(props)
      if (title) {
        document.title = title
      }
    }

    componentDidMount() {
      this.updateTitle(this.props)
    }

    componentWillReceiveProps(props) {
      this.updateTitle(props)
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }
}

