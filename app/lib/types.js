import PropTypes from 'prop-types'

export const ActionType = PropTypes.func.isRequired

export const IdType = PropTypes.string

export const LocationType = PropTypes.shape({
  latitude : PropTypes.number,
  longitude: PropTypes.number
})

export const TaskType = PropTypes.shape({
  id         : IdType,
  address    : PropTypes.string,
  service    : PropTypes.string,
  back       : PropTypes.string,
  latitude   : PropTypes.number,
  longitude  : PropTypes.number,
  description: PropTypes.string,
  createdAt  : PropTypes.number
})

export const UserType = PropTypes.shape({
  uid          : IdType,
  email        : PropTypes.string,
  emailVerified: PropTypes.bool,
  name         : PropTypes.string
})
