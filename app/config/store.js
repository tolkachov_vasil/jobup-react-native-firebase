import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducers from '../reducers'
import sagas from '../sagas'

export function configureStore(context = {}) {
  const sagaMiddleware = createSagaMiddleware()
  const rootReducer = combineReducers(reducers)

  const store = compose(
    applyMiddleware(sagaMiddleware)
  )(createStore)(
    rootReducer,
    GLOBAL.__DEV__
      ? GLOBAL.__REDUX_DEVTOOLS_EXTENSION__ && GLOBAL.__REDUX_DEVTOOLS_EXTENSION__()
      : null
  )

  let sagaTask = sagaMiddleware.run(sagas, context)

  if (module.hot) {
    const acceptCallback = () => {

      const allReducers = require('../reducers/index').default
      const nextRootReducer = combineReducers(allReducers)
      store.replaceReducer(nextRootReducer)

      const getNewSagas = require('../sagas/index').default
      sagaTask.cancel()
      sagaTask.done.then(() => {
        sagaTask = sagaMiddleware.run(function* replacedSaga(action) {
          yield getNewSagas()
        })
      })
    }
    module.hot.acceptCallback = acceptCallback
  }

  return store
}
