import firebase from 'firebase'
import ReduxSagaFirebase from 'redux-saga-firebase'

const firebaseApp = firebase.initializeApp({
  apiKey           : 'AIzaSyBRME-dF33iNZw3NOKUEgWSEVdjSz8qiaE',
  authDomain       : 'jobup-e1c2a.firebaseapp.com',
  databaseURL      : 'https://jobup-e1c2a.firebaseio.com',
  storageBucket    : 'jobup-e1c2a.appspot.com',
  projectId        : 'jobup-e1c2a',
  messagingSenderId: '231632772443'
})

export const rsf = new ReduxSagaFirebase(firebaseApp)

if (GLOBAL.__DEV__) {
  GLOBAL.rsf = rsf
  GLOBAL.firebaseApp = firebaseApp
  firebase.database.enableLogging(msg => {
    try {
      if (msg.startsWith('event: ')) {
        const [path, value] = msg.split('value:')
        console.log('[FB] ' + path.slice(7), JSON.parse(value))

      } else if (msg.startsWith('0: set ')) {
        console.log('[FB] set:', JSON.parse(msg.slice(7)))
      }
    } catch (e) {console.log('[FB] [PARSE FAIL]', msg)}
  })
}
