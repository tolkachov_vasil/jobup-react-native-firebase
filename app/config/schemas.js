import { is } from '../lib/validation'

export const taskSchema = is.shape({
  service    : is.string().required(),
  work       : is.string().required(),
  description: is.string(),
  address    : is.string().required(),
  createdAt  : is.number(),
  id         : is.string(),
  latitude   : is.number().required(),
  longitude  : is.number().required()
})

export const userSchema = is.shape({
  uid          : is.string().required(),
  name         : is.string(),
  email        : is.email().required(),
  emailVerified: is.bool(),
  password     : is.string().minLength(6).required(),
  password2    :
    is.string()
      .minLength(6)
      .testRoot((root, me) => me === root.password, 're-type error')
      .required()
})

