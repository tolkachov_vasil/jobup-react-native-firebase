import firebase from 'firebase'
import { NativeModules, Platform } from 'react-native'

export const devInit = () => {
  GLOBAL.navigator.userAgent = 'react-native'

  console.disableYellowBox = true
  Platform.OS === 'ios' && NativeModules.DevSettings.setIsDebuggingRemotely(true)

  GLOBAL.firebase = firebase

}
