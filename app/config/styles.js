export const size = {
  marker      : 30,
  borderRadius: 10,
  font        : {
    smallest: 10,
    small   : 12,
    smaller : 13,
    default : 14,
    larger  : 16,
    large   : 18,
    largest : 20
  }
}

export const color = {
  active    : '#4c70fe',
  activeTask: '#d3dcff',
  panelBg   : '#fff',
  gray      : '#909090',
  red       : '#900200',
  success   : '#c0ffa1',
  fail      : '#fbb7a3'
}
