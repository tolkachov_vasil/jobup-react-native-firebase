export const TAB = {
  TASK    : 'Task',
  TASKS   : 'Tasks',
  MAP     : 'Map',
  PROFILE : 'Profile',
  LOGIN   : 'Login',
  REGISTER: 'Register',
  WELCOME : 'Welcome'
}
