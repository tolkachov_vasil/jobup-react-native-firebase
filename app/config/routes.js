export const routes = {
  tasks        : '/tasks/:userId',
  task         : '/tasks/:userId/:id',
  userDelete   : '/users/:uid/_delete',
  taskCreatedAt: '/tasks/:userId/:id/createdAt'
}
