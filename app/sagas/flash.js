import { delay } from 'redux-saga'
import { all, call, put, takeLatest } from 'redux-saga/effects'
import { addFlash, FLASH__SHOW, hideFlash } from '../actions/flash'
import { FLASH_TIME_MS } from '../config/flash'

function* createFlash({ text, kind }) {
  const id = new Date().getTime()

  yield put(addFlash(kind, text, id))
  yield call(delay, FLASH_TIME_MS)
  yield put(hideFlash(id))
}

export function* saga() {
  yield all([
    takeLatest(FLASH__SHOW, createFlash)
  ])
}
