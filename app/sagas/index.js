import { all } from 'redux-saga/effects'
import { saga as dialog } from './dialog'
import { saga as flash } from './flash'
import { saga as map } from './map'
import { saga as tabs } from './tabs'
import { saga as task } from './task'
import { saga as tasks } from './tasks'
import { saga as user } from './user'

export default function* rootSaga() {
  yield all([
    tabs(),
    map(),
    task(),
    tasks(),
    user(),
    flash(),
    dialog()
  ])
}
