import { AsyncStorage } from 'react-native'
import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import { showFlash } from '../actions/flash'
import { updateTask } from '../actions/task'
import { getTasksRequest, updateTasks } from '../actions/tasks'
import {
  updateUser,
  updateUserErrors,
  USER__AUTOLOGIN_REQUEST,
  USER__DELETE_REQUEST,
  USER__LOGIN_REQUEST,
  USER__LOGOUT_REQUEST,
  USER__REGISTER_REQUEST,
  USER__RESET_PASSWORD_REQUEST,
  USER__SEND_CONFIRMATION_REQUEST
} from '../actions/user'
import { rsf } from '../config/firebase'
import { SUCCESS } from '../config/flash'
import { routes } from '../config/routes'
import { userSchema } from '../config/schemas'
import { userDTO } from '../lib/DTO'
import { getFrom, isEmpty } from '../lib/object'
import { path } from '../lib/path'
import { attempt, confirmed } from '../lib/saga'
import { initialTask } from '../reducers/task'
import { initialUser } from '../reducers/user'

function* getLocalUserKey() {
  const keys = yield attempt(AsyncStorage.getAllKeys)

  return attempt.ok
    ? keys.find(k => k.startsWith('firebase:authUser:'))
    : null
}

function* getLocalUser() {
  const key = yield call(getLocalUserKey)

  if (key) {
    const user = yield attempt(AsyncStorage.getItem, key)

    return attempt.ok
      ? userDTO(JSON.parse(user))
      : {}
  }
  return {}
}

function* autoLogin({ next }) {
  const user = yield call(getLocalUser)

  yield put(updateUser(user))
  yield user.emailVerified && put(getTasksRequest())
  next && next()
}

function* login({ next }) {
  const { user: { email, password, errors } } = yield select()
  const loginErrors = getFrom(errors, 'email password')

  if (isEmpty(loginErrors)) {
    const data = yield attempt(rsf.auth.signInWithEmailAndPassword, email, password)
    const user = attempt.ok
      ? userDTO(data)
      : initialUser

    yield put(updateUser(user))
    yield user.emailVerified && put(getTasksRequest())
    next && next()
  }
}

function* logout({ next }) {
  yield attempt(rsf.auth.signOut)
  yield put(updateUser(initialUser))
  yield put(updateTask(initialTask))
  yield put(updateTasks({}))
  next && next()
}

function* register({ next }) {
  const { user: { errors, email, password } } = yield select()
  const registerErrors = getFrom(errors, 'email password password2')

  if (isEmpty(registerErrors)) {
    yield attempt(rsf.auth.createUserWithEmailAndPassword, email, password)
    yield attempt.ok && call(sendEmailVerification)
    next && next()
  }
}

function* resetPassword({ next }) {
  const { user: { errors, email } } = yield select()
  const resetErrors = getFrom(errors, 'email')

  if (isEmpty(resetErrors)) {
    yield attempt(rsf.auth.sendPasswordResetEmail, email)
    yield put(showFlash(SUCCESS, `Instructions sent on ${email}`))

    next && next()
  }
}

function* validateUser() {
  const { user } = yield select()
  const errors = userSchema.getErrors(user)

  yield put(updateUserErrors(errors))
}

function* sendEmailVerification() {
  const { user: { email } } = yield select()
  yield attempt(rsf.auth.sendEmailVerification)
  yield attempt.ok && put(showFlash(SUCCESS, `Instructions sent on ${email}`))
}

function* deleteAccount() {
  if (yield call(confirmed)) {
    const { user: { uid } } = yield select()
    yield attempt(rsf.database.update, path(routes.userDelete, { uid }), true)
    yield logout({})
  }
}

export function* saga() {
  yield all([
    takeLatest(USER__LOGIN_REQUEST, validateUser),
    takeLatest(USER__REGISTER_REQUEST, validateUser),
    takeLatest(USER__RESET_PASSWORD_REQUEST, validateUser),

    takeLatest(USER__AUTOLOGIN_REQUEST, autoLogin),
    takeLatest(USER__LOGIN_REQUEST, login),
    takeLatest(USER__LOGOUT_REQUEST, logout),
    takeLatest(USER__REGISTER_REQUEST, register),
    takeLatest(USER__RESET_PASSWORD_REQUEST, resetPassword),
    takeLatest(USER__SEND_CONFIRMATION_REQUEST, sendEmailVerification),
    takeLatest(USER__DELETE_REQUEST, deleteAccount)
  ])
}
