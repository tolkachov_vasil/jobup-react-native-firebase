import { all, call, select, takeLatest } from 'redux-saga/effects'
import { TASK__UPDATE } from '../actions/task'
import { USER__UPDATE } from '../actions/user'
import { TAB } from '../config/tabs'
import { setTab } from '../lib/saga'

function* toggleProfile({ value }) {
  const { user } = yield select()
  const newUser = { ...user, ...value }
  const { emailVerified, uid } = newUser

  yield call(setTab, TAB.MAP, emailVerified)
  yield call(setTab, TAB.TASKS, emailVerified)
  yield call(setTab, TAB.REGISTER, !uid)
  yield call(setTab, TAB.LOGIN, !uid)
  yield call(setTab, TAB.PROFILE, uid)
}

function* toggleTask() {
  const task = yield select(s => s.task)

  yield call(setTab, TAB.TASK, !!task.address)
}

export function* saga() {
  yield all([
    takeLatest(USER__UPDATE, toggleProfile),

    takeLatest(TASK__UPDATE, toggleTask)
  ])
}
