import { Alert } from 'react-native'
import { all, call, takeEvery } from 'redux-saga/effects'
import { DIALOG__SHOW } from '../actions/dialog'

function* showAlert({ text, kind }) {
  console.log('text, kind', text, kind)
  yield call([Alert, Alert.alert],
    'Alert Title',
    'My Alert Msg',
    [
      { text: 'Ask me later',
        onPress: () => console.log('Ask me later pressed')
      },
      { text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel'
      },
      { text: 'OK', onPress: () => console.log('OK Pressed') }
    ],
    { cancelable: false }
  )

  // const id = new Date().getTime()
  //
  // yield put(addFlash(kind, text, id))
  // yield call(delay, FLASH_TIME_MS)
  // yield put(hideFlash(id))
}

export function* saga() {
  yield all([
    takeEvery(DIALOG__SHOW, showAlert)
  ])
}
