import { all, call, put, takeLatest } from 'redux-saga/effects'
import { showFlash } from '../actions/flash'
import {
  MAP__DETECT_LOCATION,
  MAP__GET_ADDRESS_REQUEST,
  updateLocation
} from '../actions/map'
import { updateTask } from '../actions/task'
import { FAIL } from '../config/flash'
import { getAddressByLocation, getMyLocation } from '../lib/map'

function* getAddress({ location }) {
  const { error, results } = yield call(getAddressByLocation, location)

  if (error) {
    yield put(showFlash(FAIL, error))
  } else {
    if (results[0]) {
      const address = results[0] ? results[0].formatted_address : null
      yield put(updateTask({ ...location, address }))
    }
  }
}

function* detectLocation() {
  try {
    const location = yield call(getMyLocation)
    yield put(updateLocation(location))

  } catch (error) { yield put(showFlash(FAIL, error)) }
}

export function* saga() {
  yield all([
    takeLatest(MAP__GET_ADDRESS_REQUEST, getAddress),
    takeLatest(MAP__DETECT_LOCATION, detectLocation)
  ])
}
