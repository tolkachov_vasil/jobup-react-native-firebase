import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import {
  addTask,
  deleteTask,
  TASK__DELETE_REQUEST,
  TASK__SAVE_REQUEST,
  updateTaskErrors
} from '../actions/task'
import { updateOneTask } from '../actions/tasks'
import { rsf } from '../config/firebase'
import { routes } from '../config/routes'
import { taskSchema } from '../config/schemas'
import { taskDTO } from '../lib/DTO'
import { path } from '../lib/path'
import { attempt, confirmed } from '../lib/saga'

function* save({ task, next }) {
  const userId = yield select(s => s.user.uid)
  const data = taskDTO(task)

  if (!!task.id) {
    yield call(update, { ...data, id: task.id }, userId)
  } else {
    yield call(create, data, userId)
  }

  next && next()
}

function* create(task, userId) {
  const id = yield attempt(rsf.database.create, path(routes.tasks, userId), task)
  yield put(addTask({ ...task, id, createdAt: new Date().getTime() }))
}

function* update(task, userId) {
  const id = task.id
  yield attempt(rsf.database.update, path(routes.task, { userId, id }), task)
  yield attempt.ok && put(updateOneTask(task))
}

function* remove({ task, next }) {
  const userId = yield select(s => s.user.uid)
  const id = task.id

  if (yield call(confirmed)) {
    yield attempt(rsf.database.delete, path(routes.task, { userId, id }))
    yield attempt.ok && put(deleteTask(task))
    next && next()
  }
}

function* validateTask() {
  const task = yield select(s => s.task)
  const errors = taskSchema.getErrors(task)
  yield put(updateTaskErrors(errors))
}

export function* saga() {
  yield all([
    takeLatest(TASK__SAVE_REQUEST, validateTask),
    takeLatest(TASK__SAVE_REQUEST, save),
    takeLatest(TASK__DELETE_REQUEST, remove)
  ])
}
