import { all, put, select, takeLatest } from 'redux-saga/effects'
import { TASKS__GET_REQUEST, updateTasks } from '../actions/tasks'
import { rsf } from '../config/firebase'
import { routes } from '../config/routes'
import { path } from '../lib/path'
import { attempt } from '../lib/saga'

function* fetchTasks() {
  const user = yield select(s => s.user)
  if (user.uid) {
    const tasks = yield attempt(rsf.database.read, path(routes.tasks, user.uid))
    yield attempt.ok && put(updateTasks(tasks || {}))
  } else {
    yield attempt.ok && put(updateTasks({}))
  }
}

export function* saga() {
  yield all([
    takeLatest(TASKS__GET_REQUEST, fetchTasks)
  ])
}
