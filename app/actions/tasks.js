import { createAction } from '../lib/action'

export const TASKS__GET_REQUEST = 'TASKS__GET_REQUEST'
export const TASKS__UPDATE = 'TASKS__UPDATE'
export const TASKS__UPDATE_ONE = 'TASKS__UPDATE_ONE'

export const getTasksRequest = createAction(TASKS__GET_REQUEST, 'next')
export const updateTasks = createAction(TASKS__UPDATE, 'tasks')
export const updateOneTask = createAction(TASKS__UPDATE_ONE, 'task')
