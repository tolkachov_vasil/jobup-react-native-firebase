import { createAction } from '../lib/action'

export const MAP__UPDATE_LOCATION = 'MAP__UPDATE_CENTER'
export const MAP__DETECT_LOCATION = 'MAP__DETECT_LOCATION'
export const MAP__GET_ADDRESS_REQUEST = 'MAP__GET_ADDRESS_REQUEST'

export const updateLocation = createAction(MAP__UPDATE_LOCATION, 'location')
export const detectLocation = createAction(MAP__DETECT_LOCATION)
export const getAddressRequest = createAction(MAP__GET_ADDRESS_REQUEST, 'location')
