import { createAction } from '../lib/action'

export const DIALOG__SHOW = 'DIALOG__SHOW'

export const showDialog = createAction(DIALOG__SHOW, 'content', 'noCancellable')
