import { createAction } from '../lib/action'

export const TAB__SHOW = 'TAB__SHOW'
export const TAB__HIDE = 'TAB__HIDE'

export const showTab = createAction(TAB__SHOW, 'name')
export const hideTab = createAction(TAB__HIDE, 'name')
