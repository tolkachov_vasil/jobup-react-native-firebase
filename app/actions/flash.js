import { createAction } from '../lib/action'

export const FLASH__SHOW = 'FLASH__SHOW'
export const FLASH__ADD = 'FLASH__ADD'
export const FLASH__HIDE = 'FLASH__HIDE'

export const showFlash = createAction(FLASH__SHOW, 'kind', 'text')
export const addFlash = createAction(FLASH__ADD, 'kind', 'text', 'id')
export const hideFlash = createAction(FLASH__HIDE, 'id')
