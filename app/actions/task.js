import { createAction } from '../lib/action'

export const TASK__SAVE_REQUEST = 'TASK__SAVE_REQUEST'
export const TASK__DELETE_REQUEST = 'TASK__DELETE_REQUEST'
export const TASK__UPDATE = 'TASK__UPDATE'
export const TASK__ADD = 'TASK__ADD'
export const TASK__DELETE = 'TASK__DELETE'
export const TASK__UPDATE_ERRORS = 'TASK__UPDATE_ERRORS'

export const saveTaskRequest = createAction(TASK__SAVE_REQUEST, 'task', 'next')
export const deleteTaskRequest = createAction(TASK__DELETE_REQUEST, 'task')
export const updateTask = createAction(TASK__UPDATE, 'task')
export const addTask = createAction(TASK__ADD, 'task')
export const deleteTask = createAction(TASK__DELETE, 'task')
export const updateTaskErrors = createAction(TASK__UPDATE_ERRORS, 'errors')
