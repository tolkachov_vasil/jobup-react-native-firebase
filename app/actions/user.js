import { createAction } from '../lib/action'

export const USER__LOGIN_REQUEST = 'USER__LOGIN_REQUEST'
export const USER__RESET_PASSWORD_REQUEST = 'USER__RESET_PASSWORD_REQUEST'
export const USER__SEND_CONFIRMATION_REQUEST = 'USER__SEND_CONFIRMATION_REQUEST'
export const USER__AUTOLOGIN_REQUEST = 'USER__AUTOLOGIN_REQUEST'
export const USER__LOGOUT_REQUEST = 'USER__LOGOUT_REQUEST'
export const USER__DELETE_REQUEST = 'USER__DELETE_REQUEST'
export const USER__REGISTER_REQUEST = 'USER__REGISTER_REQUEST'
export const USER__UPDATE = 'USER__UPDATE'
export const USER__UPDATE_ERRORS = 'USER__UPDATE_ERRORS'

export const loginUserRequest = createAction(USER__LOGIN_REQUEST, 'next')
export const resetPasswordRequest = createAction(USER__RESET_PASSWORD_REQUEST, 'next')
export const sendConfirmationRequest = createAction(USER__SEND_CONFIRMATION_REQUEST, 'next')
export const autoLoginUserRequest = createAction(USER__AUTOLOGIN_REQUEST, 'next')
export const logoutUserRequest = createAction(USER__LOGOUT_REQUEST, 'next')
export const registerUserRequest = createAction(USER__REGISTER_REQUEST, 'next')
export const deleteUserRequest = createAction(USER__DELETE_REQUEST, 'next')
export const updateUser = createAction(USER__UPDATE, 'value')
export const updateUserErrors = createAction(USER__UPDATE_ERRORS, 'errors')
