import { FLASH__ADD, FLASH__HIDE } from '../actions/flash'

const initialState = []

export function flash(state = initialState, action) {
  switch (action.type) {

  case FLASH__ADD: {
    const { kind, text, id } = action
    return [...state, { kind, text, id }]
  }

  case FLASH__HIDE: {
    const { id } = action
    return state.filter(f => f.id !== id)
  }

  default:
    return state
  }
}
