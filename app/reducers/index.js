import { flash } from './flash'
import { map } from './map'
import { tabs } from './tabs'
import { task } from './task'
import { tasks } from './tasks'
import { user } from './user'

export default {
  task,
  tasks,
  map,
  user,
  flash,
  tabs
}
