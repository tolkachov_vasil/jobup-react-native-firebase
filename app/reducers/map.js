import { MAP__UPDATE_LOCATION, MAP__UPDATE_MARKER } from '../actions/map'

const initialState = {
  location: {
    latitude      : 53.90646811121017,
    longitude     : 27.567404117435217,
    latitudeDelta : 0.0045297086214262094,
    longitudeDelta: 0.0047297403216290945
  }
}

export function map(state = initialState, action) {
  switch (action.type) {

  case MAP__UPDATE_LOCATION:
    return {
      ...state,
      location: {
        ...state.location, ...action.location
      }
    }

  default:
    return state
  }
}
