import { TAB__HIDE, TAB__SHOW } from '../actions/tabs'
import { TAB } from '../config/tabs'

const initialState = {
  [TAB.TASK]    : false,
  [TAB.TASKS]   : false,
  [TAB.MAP]     : false,
  [TAB.PROFILE] : false,
  [TAB.LOGIN]   : false,
  [TAB.REGISTER]: false
}

export function tabs(state = initialState, action) {
  switch (action.type) {

  case TAB__SHOW:
    return { ...state, [action.name]: true }

  case TAB__HIDE:
    return { ...state, [action.name]: false }

  default:
    return state
  }
}
