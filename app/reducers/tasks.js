import { TASK__ADD, TASK__DELETE } from '../actions/task'
import {
  TASKS__GET_REQUEST,
  TASKS__UPDATE,
  TASKS__UPDATE_ONE
} from '../actions/tasks'

const initialTasks = {
  collection: [],
  isLoading : false
}

export function tasks(state = initialTasks, action) {
  switch (action.type) {

  case TASKS__GET_REQUEST:
    return {
      ...state,
      isLoading: true
    }

  case TASKS__UPDATE:
    return {
      ...state,
      collection: Object.keys(action.tasks)
        .map(id => ({ ...action.tasks[id], id })),
      isLoading : false
    }

  case TASK__ADD:
    return {
      ...state,
      collection: [...state.collection, action.task]
    }

  case TASK__DELETE: {
    const id = action.task.id

    return {
      ...state,
      collection: [...state.collection.filter(t => t.id !== id)]
    }
  }

  case TASKS__UPDATE_ONE: {
    const id = action.task.id

    return {
      ...state,
      collection: [
        ...state.collection.filter(t => t.id !== id),
        action.task
      ]
    }
  }

  default:
    return state
  }
}
