import { USER__UPDATE, USER__UPDATE_ERRORS } from '../actions/user'
import { State } from '../lib/stater'

export const initialUser = new State({
  errors       : {},
  email        : null,
  password     : null,
  name         : null,
  uid          : null,
  emailVerified: false
})

export function user(state = initialUser, action) {
  switch (action.type) {

  case USER__UPDATE:
    return new State(action.value)

  case USER__UPDATE_ERRORS:
    return state.update('errors', action.errors)

  default:
    return state
  }
}
