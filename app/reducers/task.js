import { TASK__UPDATE, TASK__UPDATE_ERRORS } from '../actions/task'

export const initialTask = {
  id         : null,
  service    : null,
  work       : null,
  description: null,
  longitude  : null,
  latitude   : null,
  address    : null,
  errors     : {}
}

export function task(state = initialTask, action) {
  switch (action.type) {

  case TASK__UPDATE:
    return {
      ...state,
      ...action.task
    }

  case TASK__UPDATE_ERRORS:
    return {
      ...state,
      errors: action.errors
    }

  default:
    return state
  }
}
