import React from 'react'
import combineStyles from 'react-combine-styles'
import { StyleSheet, View } from 'react-native'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import { saveTaskRequest, updateTask } from '../actions/task'
import { Button, Col, Row, Show, Text, TextInput } from '../components/common'
import ServiceIcon from '../components/common/SVG/ServiceIcon'
import { color } from '../config/styles'
import { TAB } from '../config/tabs'
import { taskOptions } from '../config/task-options'
import { withProps } from '../lib/decorators'
import { taskTitle } from '../lib/task'
import { ActionType, TaskType } from '../lib/types'
import { initialTask } from '../reducers/task'

@connect(
  ({ task }) => ({ task }),
  { updateTask, saveTaskRequest })
@withNavigation
export default class TaskScreen extends React.Component {
  static propTypes = {
    task           : TaskType,
    updateTask     : ActionType,
    saveTaskRequest: ActionType
  }

  @withProps
  save({ saveTaskRequest, task }) {
    saveTaskRequest(task, () => this.close())
  }

  @withProps
  close({ updateTask, navigation }) {
    updateTask(initialTask)
    navigation.navigate(TAB.MAP)
  }

  @withProps
  render({ map, task, updateTask }) {
    const { service, work, description, address, id } = task

    return (
      <View style={styles.back}>

        <Text value={(id ? 'EDIT' : 'NEW') + ' TASK'} largest center gapBottom/>

        <Text value={taskTitle(task)} largest gapBottom/>

        <Text value={`My address is ${address}`} gapBottom/>

        <Text value='SERVICE TYPE' gray gapBottom gapTop center/>
        {this.renderServices()}

        <Show when={!!service}>
          <Text value={`${(service || '').toUpperCase()} TASKS`} gray gapBottom
                gapTop center/>
          {this.renderTasks()}
        </Show>

        <Show when={!!work}>
          <Text value='DESCRIPTION' gray gapTop center/>
          <TextInput value={description} name='description'
                     onChange={updateTask}/>
        </Show>

        <Row>
          <Col>
            <Show when={!!work}>
              <Button caption='SAVE' onPress={() => this.save()}/>
            </Show>
          </Col>
          <Col children={
            <Button caption='CANCEL' onPress={() => this.close()}/>}/>
        </Row>

      </View>
    )
  }

  @withProps
  renderServices({ updateTask, task: { service } }) {
    return (
      <Row>
        {Object.keys(taskOptions).map(name =>
          <Col key={name}>
            <Button onPress={() => updateTask({ service: name, work: null })}>
              <ServiceIcon
                name={name}
                active={service === name}
              />
            </Button>
          </Col>
        )}
      </Row>
    )
  }

  @withProps
  renderTasks({ updateTask, task: { service, work } }) {
    return (
      service &&
      <Row wrap>
        {taskOptions[service].map(name =>
          <Col key={name} mode='center'>
            <Button onPress={() => updateTask({ work: name })}>
              <View style={combineStyles({
                task      : true,
                taskActive: name === work
              }, styles)}>
                <Text value={name}/>
              </View>
            </Button>
          </Col>
        )}
      </Row>
    )
  }
}

const styles = StyleSheet.create({
  back      : {
    ...StyleSheet.absoluteFillObject,
    padding        : 20,
    backgroundColor: '#fff',
    alignSelf      : 'stretch',
    borderWidth    : 1,
    borderColor    : '#aaa'

  },
  task      : {
    padding     : 5,
    margin      : 5,
    borderRadius: 50,
    borderWidth : 2,
    borderColor : 'transparent'
  },
  taskActive: {
    borderColor: color.active
  }
})
