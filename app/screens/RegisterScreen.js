import React from 'react'
import { StyleSheet, View } from 'react-native'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import { registerUserRequest, updateUser } from '../actions/user'
import { Button, Text, TextInput } from '../components/common'
import { TAB } from '../config/tabs'
import { withProps } from '../lib/decorators'
import { ActionType, UserType } from '../lib/types'

@connect(({ user }) => ({ user }), { registerUserRequest, updateUser })
@withNavigation
export default class RegisterScreen extends React.Component {
  static propTypes = {
    user               : UserType,
    updateUser         : ActionType,
    registerUserRequest: ActionType
  }

  @withProps
  register({ registerUserRequest, navigation }) {
    registerUserRequest(() =>
      navigation.navigate(TAB.LOGIN))
  }

  // noinspection JSCheckFunctionSignatures
  @withProps
  render({ updateUser, user: { errors, email, password, password2 } }) {
    return (
      <View style={styles.back}>

        <Text value='REGISTER NEW ACCOUNT' largest center/>

        <Text value='EMAIL' gray gapTop/>
        <TextInput
          value={email}
          name='email'
          onChange={updateUser}
          errors={errors.email}
        />

        <Text value='PASSWORD' gray gapTop/>
        <TextInput
          value={password}
          password
          name='password'
          errors={errors.password}
          onChange={updateUser}
        />

        <Text value='RE-TYPE PASSWORD' gray gapTop/>
        <TextInput
          value={password2}
          password
          name='password2'
          errors={errors.password2}
          onChange={updateUser}
        />

        <Button
          caption='REGISTER'
          onPress={() => this.register()}
        />

      </View>
    )
  }
}

const styles = StyleSheet.create({
  back: {
    ...StyleSheet.absoluteFillObject, padding: 20
  }
})
