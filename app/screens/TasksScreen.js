import PropTypes from 'prop-types'
import React from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import { showDialog } from '../actions/dialog'
import { updateLocation } from '../actions/map'
import { deleteTaskRequest, updateTask } from '../actions/task'
import { Text } from '../components/common'
import Task from '../components/Task'
import { TAB as TABS } from '../config/tabs'
import { withProps } from '../lib/decorators'
import { isEmpty } from '../lib/object'
import { ActionType } from '../lib/types'

@connect(
  ({ tasks, task }) => ({ tasks, task }),
  {
    deleteTaskRequest,
    updateTask,
    updateLocation,
    showDialog
  })
@withNavigation
export default class TasksScreen extends React.Component {
  static propTypes = {
    tasks            : PropTypes.object.isRequired,
    deleteTaskRequest: ActionType,
    showDialog       : ActionType,
    updateLocation   : ActionType,
    updateTask       : ActionType
  }

  @withProps
  edit({ updateTask, navigation }, task) {
    updateTask(task)
    navigation.navigate(TABS.TASK)
  }

  @withProps
  select({ updateLocation, navigation }, task) {
    const { longitude, latitude, address } = task

    navigation.navigate(TABS.MAP)
    updateLocation({ longitude, latitude }, address)
  }

  @withProps
  remove({ deleteTaskRequest, showModal }, task) {
    deleteTaskRequest(task)
  }

  // noinspection JSCheckFunctionSignatures
  @withProps
  render({ tasks: { collection, isLoading }, deleteTaskRequest }) {
    const sorted = [...collection].sort((a, b) => a.createdAt - b.createdAt)

    return (
      isEmpty(collection) ?
      <Text
        value={isLoading ? 'Loading tasks...' : 'No tasks. Create new in Map.'}
        largest center gapTop/>
        :
      <ScrollView style={styles.back}>
        {sorted.map(task =>
          <Task
            key={task.id}
            value={task}
            onDelete={() => this.remove(task)}
            onSelect={() => this.select(task)}
            onEdit={() => this.edit(task)}
          />
        )}
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  back: {
    marginBottom: 40
  }
})
