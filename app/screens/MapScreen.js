import PropTypes from 'prop-types'
import React from 'react'
import { withNavigation } from 'react-navigation'
import { connect } from 'react-redux'
import { getAddressRequest } from '../actions/map'
import { updateTask } from '../actions/task'
import GMap from '../components/GMap'
import { TAB as TABS, TAB } from '../config/tabs'
import { withProps } from '../lib/decorators'
import { ActionType } from '../lib/types'

@connect(
  ({ map, tasks }) => ({ map, tasks }),
  { getAddressRequest, updateTask })
@withNavigation
export default class MapScreen extends React.Component {
  static propTypes = {
    map              : PropTypes.object.isRequired,
    tasks            : PropTypes.object.isRequired,
    getAddressRequest: ActionType,
    updateTask       : ActionType
  }

  @withProps
  requestAddress({ navigation, getAddressRequest }, location) {
    getAddressRequest(location)
    navigation.navigate(TAB.TASK)
  }

  @withProps
  onSelect({ getAddressRequest, updateTask, navigation }, { location, marker }) {
    if (location) {
      getAddressRequest(location)
    } else if (marker) {
      updateTask(marker)
      navigation.navigate(TABS.TASK)
    }
  }

  @withProps
  render({ map: { location }, tasks: { collection } }) {
    return (
      <GMap
        location={location}
        markers={collection}
        onSelect={d => this.onSelect(d)}
      />
    )
  }
}
