import React from 'react'
import { StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import {
  autoLoginUserRequest,
  deleteUserRequest,
  logoutUserRequest,
  sendConfirmationRequest
} from '../actions/user'
import { Button, Hide, Row, Text } from '../components/common'
import { withProps } from '../lib/decorators'
import { ActionType, UserType } from '../lib/types'

@connect(
  ({ user }) => ({ user }),
  {
    logoutUserRequest,
    autoLoginUserRequest,
    deleteUserRequest,
    sendConfirmationRequest
  })
export default class ProfileScreen extends React.Component {
  static propTypes = {
    user                   : UserType,
    logoutUserRequest      : ActionType,
    autoLoginUserRequest   : ActionType,
    sendConfirmationRequest: ActionType,
    deleteUserRequest      : ActionType
  }

  // noinspection JSCheckFunctionSignatures
  @withProps
  render({
           user,
           logoutUserRequest,
           autoLoginUserRequest,
           sendConfirmationRequest,
           deleteUserRequest
         }) {
    return (
      <View style={styles.back}>

        <Text value='MY PROFILE' largest center/>

        <Text value='EMAIL' gray gapBottom gapTop/>
        <Text value={user.email} gapBottom/>

        <Hide when={user.emailVerified}>
          <Text value='Email is not verified!' red gapBottom/>
        </Hide>

        <Row wrap>

          <Button caption='LOGOUT' onPress={logoutUserRequest}/>

          <Hide when={user.emailVerified}>
            <Button caption='UPDATE' onPress={autoLoginUserRequest}/>
            <Button caption='RE-SEND CONFIRMATION EMAIL'
                    onPress={sendConfirmationRequest}/>
          </Hide>

          <Button caption='DELETE ACCOUNT' red onPress={deleteUserRequest}/>

        </Row>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  back: {
    ...StyleSheet.absoluteFillObject,
    padding: 20
  }
})
