import React from 'react'
import { StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import {
  loginUserRequest,
  resetPasswordRequest,
  updateUser
} from '../actions/user'
import { Button, Row, Text, TextInput } from '../components/common'
import { withProps } from '../lib/decorators'
import { ActionType, UserType } from '../lib/types'

@connect(
  ({ user }) => ({ user }),
  { loginUserRequest, resetPasswordRequest, updateUser })
export default class LoginScreen extends React.Component {
  static propTypes = {
    user                : UserType,
    loginUserRequest    : ActionType,
    resetPasswordRequest: ActionType,
    updateUser          : ActionType
  }

  // noinspection JSCheckFunctionSignatures
  @withProps
  render({
           loginUserRequest,
           resetPasswordRequest,
           showDialog,
           updateUser,
           user: { email, password, errors }
         }) {
    return (
      <View style={styles.back}>

        <Text value='LOGIN' largest center/>

        <Text value='EMAIL' gray gapTop/>
        <TextInput
          value={email}
          name='email'
          onChange={updateUser}
          errors={errors.email}
        />

        <Text value='PASSWORD' gray gapTop/>
        <TextInput
          password
          value={password}
          name='password'
          errors={errors.password}
          onChange={updateUser}
        />

        <Row>
          <Button
            caption='LOGIN'
            onPress={loginUserRequest}
          />
          <Button
            caption='RESET PASSWORD'
            onPress={resetPasswordRequest}
          />
        </Row>


      </View>
    )
  }
}

const styles = StyleSheet.create({
  back: {
    ...StyleSheet.absoluteFillObject,
    padding        : 20,
    backgroundColor: '#fff'
  }
})
