import React from 'react'
import { Text } from '../components/common'

export default class SplashScreen extends React.Component {
  static propTypes = {}

  render() {
    return (
      <Text value={'JobUp'} largest center gapTop/>
    )
  }
}
