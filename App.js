import React from 'react'
import { Provider } from 'react-redux'
import { devInit } from './app/config/development'
import './app/config/firebase'
import { configureStore } from './app/config/store'
import Layout from './app/Layout'

if (GLOBAL.__DEV__) {
  devInit()
}

export default class App extends React.Component {
  state = { store: configureStore() }

  render() {
    return (
      <Provider store={this.state.store}>
        <Layout/>
      </Provider>
    )
  }
}
